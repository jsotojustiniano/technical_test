const { Schema, model } = require('mongoose');

const productoSchema = new Schema({
    nombre: { type: String, required: true },
    descripcion: { type: String, required: true }
}, {
    timestamps: true,
    versionKey: false,
    collection: 'productos'
})

module.exports = model('Producto', productoSchema)