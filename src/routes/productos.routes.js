const { Router } = require('express')
const router = Router();
const ctrlProduct = require('../controllers/products.controller')

router.get('/', ctrlProduct.getProducts)
router.post('/', ctrlProduct.createProduct)


module.exports = router;