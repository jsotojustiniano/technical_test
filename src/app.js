const express = require('express')
const app = express()
var cors = require('cors')

require('./utils/connection');

app.use(cors({ origin: true, credentials: true }));

app.use(express.urlencoded({ extended: true }))

// para entienda el formato que le envia el cliente JSON
app.use(express.json())

app.use('/product', require('./routes/productos.routes'))

module.exports = app;