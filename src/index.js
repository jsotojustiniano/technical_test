const app = require('./app')
require('dotenv').config();

const port = process.env.PORT || 3000

app.listen(process.env.PORT, () => {
    console.log(`server on port ${port}`)
})