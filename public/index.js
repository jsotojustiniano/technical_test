var myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");

// const 
const name = document.getElementById('name')
const description = document.getElementById('description')
const enviar = document.getElementById('enviar')


enviar.addEventListener('click', (e) => {
    e.preventDefault();
    const data = {
        "nombre": name.value,
        "descripcion": description.value
    }

    console.log(data)

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: JSON.stringify(data),
        redirect: 'follow'
    };

    fetch("http://localhost:3000/product", requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));

    // console.log(data)
})


// var raw = JSON.stringify({ "nombre": "DASFsaf", "descripcion": "dsadsd", "edad": "dsadsad", "tipo": "dasdsad", "fecha_nacimiento": "10-03-12", "raza": "dsadsad", "red_social": "dasdsad", "sexo": "M", "cliente_codigo": "" });

// var requestOptions = {
//     method: 'POST',
//     headers: myHeaders,
//     body: raw,
//     redirect: 'follow'
// };

// fetch("http://localhost:3000/product", requestOptions)
//     .then(response => response.text())
//     .then(result => console.log(result))
//     .catch(error => console.log('error', error));